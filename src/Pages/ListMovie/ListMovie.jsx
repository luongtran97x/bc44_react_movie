import { render } from "@testing-library/react";
import { Card, Col, Row } from "antd";

import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { https } from "../service/config";

const { Meta } = Card;
export default function ListMovie() {
  const [movieArr, setMovieArr] = useState([]);

  useEffect(() => {
    https
      .get("api/QuanLyPhim/LayDanhSachPhim?maNhom=GP08")
      .then((res) => {
        console.log("🚀 ~ res:", res);
        setMovieArr(res.data.content);
      })
      .catch((err) => {
        console.log("🚀 ~ err:", err);
      });
  }, []);

  let renderMovieList = () => {
    return movieArr.map(({hinhAnh,tenPhim,maPhim}) => {
        return  <Card 
        className="shadow-xl"
        key={maPhim}
        hoverable
        style={{
          width: 240,
        }}
        cover={
          <img
          className="h-60 object-cover"
            alt="example"
            src={hinhAnh}
          />
        }
      >
        <Meta className="text-center" title={tenPhim} />
        <NavLink className="w-full inline-block text-center rounded-lg bg-red-500 mt-3 transition duration-500 hover:scale-75 cursor-pointer " to={`/detail/${maPhim}`} >Xem phim</NavLink>
      </Card>;
    });
  };

  return <div className="container grid grid-cols-4 gap-5">
        {renderMovieList()}
    </div>;
}
