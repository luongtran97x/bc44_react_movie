import React from 'react'
import Header from '../../Components/Header/Header'
import ListMovie from '../ListMovie/ListMovie'
import TabsMovie from './TabsMovie/TabsMovie'

export default function HomePage() {
  return (
    <div className='space-y-5'>
      <Header></Header>
      <ListMovie></ListMovie>
      <TabsMovie></TabsMovie>
    </div>
  )
}
