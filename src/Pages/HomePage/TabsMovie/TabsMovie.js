import React, { useEffect, useState } from "react";
import { Tabs } from "antd";
import { https } from "../../service/config";

const onChange = (key) => {
  console.log(key);
};
const items = [
  {
    key: "1",
    label: `Tab 1`,
    children: `Content of Tab Pane 1`,
  },
  {
    key: "2",
    label: `Tab 2`,
    children: `Content of Tab Pane 2`,
  },
  {
    key: "3",
    label: `Tab 3`,
    children: `Content of Tab Pane 3`,
  },
];
export default function TabsMovie() {
  const [heThongRap, setHeThongRap] = useState([]);

  useEffect(() => {
    https
      .get("api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP08")
      .then((res) => {
        console.log("🚀 ~ res:", res);
        setHeThongRap(res.data.content);
      })
      .catch((err) => {
        console.log("🚀 ~ err:", err);
      });
  }, []);
  let renderDsPhim= (danhSachPhim =[]) => {
    return danhSachPhim.map((item) => {
      return(
        <div>
          <img src={item.hinhAnh} className="w-32 h-40 object-cover" alt="" />
        </div>
      )
        
      
    }
    )
  }
  
  let renderHeThongRap = () => {
    return heThongRap.map((heThong, index) => {
      console.log("🚀 ~ heThong:", heThong)
      return {
        key: index,
        label: <img src={heThong.logo} width={100} alt="" />,
        children: <Tabs 
        defaultActiveKey="1"
        tabPosition="left"
        items={heThong.lstCumRap.map((cumRap,index) => {
          console.log("🚀 ~ cumRap:", cumRap)
          if(cumRap.length !== 0){
            return {
              key:cumRap.maCumRap,
              label:<div className="text-left w-80 whitespace-normal">
                <p className="text-green-600 font-bold">{cumRap.tenCumRap} </p>
                <p className="truncate">{cumRap.diaChi}</p>
              </div>,
             children: renderDsPhim(cumRap.danhSachPhim)
            }
          }
         
        }
        )}
        />,
      };
    });
  };

  return (
    <div className="container">
      <Tabs
        defaultActiveKey="1"
        tabPosition="left"
        items={renderHeThongRap()}
        onChange={onChange}
      />
    </div>
  );
}
