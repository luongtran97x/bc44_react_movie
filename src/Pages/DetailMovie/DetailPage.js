import { Progress } from "antd";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { https } from "../service/config";

export default function DetailPage() {
  // userParam lay url browser
  const [movie,setMovie] = useState({})
  let { id } = useParams();
  useEffect(() => {
    https
      .get(`api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`)
      .then((res) => {
        setMovie(res.data.content)
      })
      .catch((err) => {
      });
  }, []);
  return <div className="container flex justify-center items-center space-x-5">
    <h2>{movie.tenPhim} </h2>
    <img width={300} src={movie.hinhAnh} alt={movie.biDanh} />
    <Progress type="circle" percent={movie.danhGia *10 } 
    format={(percent)=>`${percent/10} Điểm`}
    ></Progress>
    </div>;
}
