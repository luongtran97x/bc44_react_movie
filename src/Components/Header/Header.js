import React from "react";
import { NavLink } from "react-router-dom";
import UserNav from "./UserNav";

export default function Header() {
  return (
    <div>
  
      <div className="container h-20 flex justify-between items-center shadow">
        <span className="text-2xl text-red-500 font-bold">CyberFlix</span>
       <UserNav/>
      </div>
    </div>
  );
}
