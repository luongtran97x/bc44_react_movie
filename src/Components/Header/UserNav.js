import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { localService } from "../../Pages/service/localStoreService";

export default function UserNav() {
  let user = useSelector((state) => state.userSlice.userInfo);
  console.log("🚀 ~ user:", user);
  let btnClass = "px-5 py-2 rounded border border-black text-blue";
  let renderContent = () => {
    if (user) {
      return (
        <>
          <span>{user.hoTen} </span>
          <button 
            onClick={handelLogout}
          
          className={btnClass}> Log Out </button>
        </>
      );
    } else {
      return (
        <>
          {/* <NavLink to={"/login"}>
            <button className={btnClass}> Login</button>
          </NavLink> */}
          <button onClick={()=>{
            window.location.href="/login"
          }} className={btnClass}> Login</button>

          <button className={btnClass}> Resgister</button>
        </>
      );
    }
  };
  let handelLogout = () => {
    localService.removeUser()
    window.location.reload()
  }
  
  return <div className="flex items-center space-x-5">{renderContent()}</div>;
}
