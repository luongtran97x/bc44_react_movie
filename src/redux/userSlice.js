import { createSlice } from "@reduxjs/toolkit";
import { localService } from "../Pages/service/localStoreService";

const initialState = {
  userInfo: localService.getUser(),
};

const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setLogin: (state, action) => {
      state.userInfo = action.payload;
    },
  },
});

export const { setLogin } = userSlice.actions;

export default userSlice.reducer;
