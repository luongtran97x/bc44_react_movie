import logo from "./logo.svg";
import "./App.css";
import HomePage from "./Pages/HomePage/HomePage";
import { BrowserRouter, Navigate, Route, Router, Routes } from "react-router-dom";
import LoginPage from "./Pages/LoginPage/LoginPage";

import DetailPage from "./Pages/DetailMovie/DetailPage";
import NotFoundPage from "./Pages/404Page/NotFoundPage";
function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomePage />}></Route>
          <Route path="/login" element={<LoginPage />}></Route>
          <Route path="/detail/:id" element={<DetailPage />}></Route>
          <Route path="/404" element={<NotFoundPage/>} ></Route>
          <Route path="*" element={<Navigate to="/404" />}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
